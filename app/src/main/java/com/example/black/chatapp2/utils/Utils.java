package com.example.black.chatapp2.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.black.chatapp2.R;

/**
 * Created by Black on 18.01.2016.
 */
public class Utils {

    public static AlertDialog showDialog(Context context, int msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Error");
        builder.setMessage(msg);
        AlertDialog alert = builder.create();
        alert.show();
        return alert;
    }
}
