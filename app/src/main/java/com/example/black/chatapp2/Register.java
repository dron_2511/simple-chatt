package com.example.black.chatapp2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.black.chatapp2.utils.Utils;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by Black on 18.01.2016.
 */
public class Register extends AppCompatActivity implements View.OnClickListener {

    private EditText user;
    private EditText pwd;
    private EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        user = (EditText) findViewById(R.id.et_userName);
        pwd = (EditText) findViewById(R.id.et_password);
        email = (EditText) findViewById(R.id.et_email);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnReg2) {
            String u = user.getText().toString();
            String p = pwd.getText().toString();
            String e = email.getText().toString();
            if (u.length() == 0 || p.length() == 0 || e.length() == 0) {
                Utils.showDialog(this, R.string.err_fields_empty);
                return;
            }
            final ProgressDialog dia = ProgressDialog.show(this, null,
                    getString(R.string.alert_wait));

            final ParseUser pu = new ParseUser();
            pu.setEmail(e);
            pu.setPassword(p);
            pu.setUsername(u);
            pu.signUpInBackground(new SignUpCallback() {

                @Override
                public void done(ParseException e) {
                    dia.dismiss();
                    if (e == null) {
                        UserList.user = pu;
                        startActivity(new Intent(Register.this, UserList.class));
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        Utils.showDialog(Register.this, R.string.err_singup);
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
